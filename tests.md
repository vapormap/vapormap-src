# Test pouvant être réalisés sur l'application

---
## API - Qualité de code 

La version de Python supportée dans Ubuntu 24.04 est  Python 3.12.3

La configuration des linter python se trouve dans le fichier `api/pyproject.toml`.

Pour assurer la cohérence entre les linters, la longueur maximum des lignes a été définie à 120 caractères

### Flake8

``` bash
cd api
docker run -it --rm -v $(pwd):/app -w /app/api  python:3.12-slim bash
pip install flake8 flake8-pyproject
flake8 app
```

### Black

``` bash
cd api
docker run -it --rm -v $(pwd):/app -w /app/api python:3.12-slim bash
pip install black
black --diff app
```

### Pylint

``` bash
cd api
docker run -it --rm -v $(pwd):/app  -w /app python:3.12-slim bash
pip install pylint
pip install -r requirements/development.txt
pylint app
```


---
## Frontend - Qualité de code 


### Code HTML : HTMLHint

- [https://htmlhint.com/docs/user-guide/getting-started](https://htmlhint.com/docs/user-guide/getting-started)

La configuration de HtmlHint se trouve dans le fichier `frontend/.htmlhintrc`

``` bash
docker run -it --rm -v $(pwd):/app -w /app/frontend   node:lts-slim bash
npm install -g htmlhint
npx htmlhint html/index.html
```

### Code javascript : jshint

- [https://jshint.com/docs/options/](https://jshint.com/docs/options/)

La configuration de jsHint se trouve dans le fichier `frontend/.jshintrc`

``` bash
cd frontend
docker run -it --rm -v $(pwd):/app  -w /app  node:lts-slim bash
npm install -g jshint
jshint --extract=auto html/index.html
```


---
## Test de code pour l'API

``` bash
cd api
docker run -it --rm -v $(pwd):/app  -w /app/api python:3.12-slim bash
pip install pytest 
pip install -r requirements/development.txt
cd app
pytest
```

``` bash
cd api
docker run -it --rm -v $(pwd):/app -w /app/api python:3.12-slim bash
pip install pytest coverage pytest-cov
pip install -r requirements/development.txt
cd app
pytest --cov
```

---
## Tests fonctionnels

- Lancement de conteneur
``` bash
docker run -it --rm -v $(pwd):/app  -p 5000:5000 -p 8000:8000  python:3.12-slim bash
```
- Lancement de l'API
``` bash
cd /app/api
pip install -r requirements/development.txt
export SETTINGS_FILE="development"
export FLASK_APP=app
cd app
flask db upgrade
flask run    -h 0.0.0.0 -p 5000 &
```
-  Lancement du Frontend
``` bash
cd /app/frontend
echo '{ "apiUrl": "http://localhost:5000" }' > html/config.json
cd html
python -m http.server &
```
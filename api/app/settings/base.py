# base.py

"""
Base configuration settings for the application.

Attributes:
    ALLOWED_CORS_ORIGINS (str): Specifies the origins that are allowed
        to make cross-origin requests.
        A value of "*" allows requests from any origin.
"""
import os

# Get current folder absolute PATH
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

ALLOWED_CORS_ORIGINS = "*"

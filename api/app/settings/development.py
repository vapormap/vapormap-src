"""
Development configuration settings for the application.

This module sets up the secret key and database connection
 for the development environment.

Attributes:
    SECRET_KEY (str): The secret key for the application,
                      used for session management and security.
    SQLALCHEMY_DATABASE_URI (str): The URI for connecting to the SQLite
                                   database used in development.
"""
from app.settings.base import BASE_DIR
import os

SECRET_KEY = "testseckey"

# Set database URI
SQLALCHEMY_DATABASE_URI = f"sqlite:///{os.path.join(BASE_DIR, 'datas', 'db.sqlite3')}"

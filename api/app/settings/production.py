"""
Production configuration settings for the application.

This module sets up the database connection and secret key for the production
environment using environment variables.
If the environment variables are not set, default values are used.

Attributes:
    dbuser (str): The database username, default is "user_vapormap".
    password (str): The database password, default is "vapormap".
    dbhost (str): The database host, default is "localhost".
    dbname (str): The database name, default is "db_vapormap".
    SECRET_KEY (str): The secret key for the application,
                      default is a random string.
    SQLALCHEMY_DATABASE_URI (str): The URI for connecting to the database.
"""

import os

dbuser = os.environ.get("VAPOR_DBUSER", "user_vapormap")
password = os.environ.get("VAPOR_DBPASS", "vapormap")
dbhost = os.environ.get("VAPOR_DBHOST", "localhost")
dbname = os.environ.get("VAPOR_DBNAME", "db_vapormap")

SECRET_KEY = os.environ.get(
    "VAPOR_SECRET_KEY", "gjhliksqsdfghsdgfbhsdkjgnlkdsfj:nglbksjdhnbk"
)

SQLALCHEMY_DATABASE_URI = f"mysql://{dbuser}:{password}@{dbhost}/{dbname}"

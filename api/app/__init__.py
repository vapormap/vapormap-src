"""
This module initializes the package and defines the version of the package.

Attributes:
    __version__ (str): The current version of the package.
"""

__version__ = "0.1.0"

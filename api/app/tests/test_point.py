# tests/test_point.py

import pytest

from datetime import datetime, timezone
from app.app import app, Point
from flask_sqlalchemy import SQLAlchemy


@pytest.fixture(scope="module")
def test_client():
    """
    Fixture for setting up the Flask test client.
    """
    flask_app = app

    # Configure the app for testing
    flask_app.config["TESTING"] = True
    flask_app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///:memory:"

    # Create a test client
    testing_client = flask_app.test_client()

    # Establish an application context
    ctx = flask_app.app_context()
    ctx.push()

    # Create the database and the database table(s)
    db = SQLAlchemy(app)
    db.create_all()

    yield testing_client  # this is where the testing happens!

    # Tear down the database
    db.session.remove()
    db.drop_all()
    ctx.pop()


@pytest.fixture(scope="function")
def test_point_creation(new_point):
    """
    Test the creation of a Point object.
    """
    assert new_point.element_name == "Test Element"
    assert new_point.longitude == 10.0
    assert new_point.latitude == 20.0
    assert new_point.comment == "Test Comment"
    assert isinstance(new_point.date, datetime.now(timezone.utc))


@pytest.fixture(scope="function")
def test_add_point_to_db(test_client, new_point, add_point_to_db):
    """
    Test adding a Point object to the database.
    """
    db = SQLAlchemy(app)
    added_point = add_point_to_db(new_point)
    assert added_point in db.session


@pytest.fixture(scope="function")
def test_get_point(test_client, new_point, add_point_to_db):
    """
    Test retrieving a Point object from the database.
    """
    added_point = add_point_to_db(new_point)
    retrieved_point = Point.query.get(added_point.id)
    assert retrieved_point == added_point


@pytest.fixture(scope="function")
def test_update_point_comment(test_client, new_point, add_point_to_db):
    """
    Test updating the comment of a Point object.
    """
    db = SQLAlchemy(app)
    added_point = add_point_to_db(new_point)
    added_point.update_comment("Updated Comment")
    db.session.commit()
    updated_point = Point.query.get(added_point.id)
    assert updated_point.comment == "Updated Comment"


@pytest.fixture(scope="function")
def test_get_coordinates(test_client, new_point):
    """
    Test retrieving the coordinates of a Point object.
    """
    assert new_point.get_coordinates() == (10.0, 20.0)

# tests/conftest.py

import pytest
from datetime import datetime, timezone
from app.app import app, Point
from flask_sqlalchemy import SQLAlchemy


@pytest.fixture(scope="module")
def test_client():
    """
    Fixture for setting up the Flask test client.
    """
    flask_app = app

    # Configure the app for testing
    flask_app.config["TESTING"] = True
    flask_app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///:memory:"

    # Create a test client
    testing_client = flask_app.test_client()

    # Establish an application context
    ctx = flask_app.app_context()
    ctx.push()

    # Create the database and the database table(s)
    db = SQLAlchemy(app)
    db.create_all()

    yield testing_client  # this is where the testing happens!

    # Tear down the database
    db.session.remove()
    db.drop_all()
    ctx.pop()


@pytest.fixture(scope="function")
def new_point():
    """
    Fixture for creating a new Point object.
    """
    return Point(
        element_name="Test Element",
        longitude=10.0,
        latitude=20.0,
        comment="Test Comment",
        date=datetime.now(timezone.utc),
    )


@pytest.fixture(scope="function")
def add_point_to_db():
    """
    Fixture for adding a Point object to the database.
    """

    def _add_point(point):
        db = SQLAlchemy(app)
        db.session.add(point)
        db.session.commit()
        return point

    return _add_point

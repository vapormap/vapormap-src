"""
This module sets up the Flask application, including configuration,
 database, and API endpoints.

It initializes:
- Flask application with configurations from environment-specific
   settings files.
- Cross-Origin Resource Sharing (CORS) settings.
- SQLAlchemy for ORM.
- Flask-Migrate for database migrations.
- Marshmallow for serialization/deserialization.
- Flask-Restful for creating RESTful APIs.

It also defines the `Point` model and its schema, as well as a resource for
 handling point-related API requests.
"""

import os

from datetime import datetime, timezone

from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import SQLAlchemyError, IntegrityError
from flask_migrate import Migrate

from flask_restful import Resource, Api
from flask_marshmallow import Marshmallow

from flask_cors import CORS

import geojson


# Define app
app = Flask(__name__)

env_settings_file = os.environ.get("SETTINGS_FILE", "development")
app.logger.warning("Override base configuration with %s.py", env_settings_file)
app.config.from_pyfile("settings/base.py")
app.config.from_pyfile(f"settings/{env_settings_file}.py")

# Cross-Origins features
CORS(app)
allowed_origins = app.config["ALLOWED_CORS_ORIGINS"]
app.logger.warning("CORS Allowed domains: %s", allowed_origins)
cors = CORS(app, resources={r"/*": {"origins": app.config["ALLOWED_CORS_ORIGINS"]}})

# Init ORM modules
db = SQLAlchemy(app)
migrate = Migrate(app, db)

# Marshmallow as (de-|)serializer
ma = Marshmallow(app)
# Flask-Restful as API generator
api = Api(app)


class Point(db.Model):
    """
    Un Point est un objet dans la base donnée, un point est définit par
    latitude : compris entre 90 et -90, 0 à l'équateur
    longitude : compris entre 180 et -180, 0 à Greenwich
    element_name : Nom de l'objet a marquer
    comment : Non utilisé
    date : Date de la marque
    """

    id = db.Column("point_id", db.Integer, primary_key=True)
    element_name = db.Column(db.String(128), nullable=False)
    longitude = db.Column(db.Float, nullable=False)
    latitude = db.Column(db.Float, nullable=False)
    comment = db.Column(db.String(128))
    date = db.Column(
        db.DateTime,
        default=datetime.now(timezone.utc),
        onupdate=datetime.now(timezone.utc),
        nullable=False,
    )

    def __repr__(self):
        return f"<Point {self.id}>"

    def __str__(self):
        return f"{self.element_name} ({self.longitude}, {self.latitude})"


class PointSchema(ma.Schema):
    """
    Schema for serializing and deserializing Point objects.

    This schema defines the fields that will be included when converting
    Point objects to and from JSON format.

    Attributes:
        id (int): The unique identifier of the point.
        element_name (str): The name of the element associated with the point.
        longitude (float): The longitude of the point.
        latitude (float): The latitude of the point.
        comment (str): An optional comment about the point.
        date (datetime): The date and time when the point was created
                         or last updated.
    """
    class Meta:
        """
        Meta class for PointSchema.
    
        This class defines the fields that will be included when serializing
        and deserializing Point objects.
    
        Attributes:
            fields (tuple): A tuple of field names to include in the serialization.
        """
        fields = ("id", "element_name", "longitude", "latitude", "comment", "date")


class PointListResource(Resource):
    """
    Resource for handling API requests related to Point objects.

    Methods:
        get: Retrieves all points from the database
             and returns them in JSON format.
        post: Creates a new point from the provided form data
              and stores it in the database.
    """

    def get(self):
        """
        Retrieve all points from the database.

        Returns:
            A list of points in JSON format.
        """
        points = Point.query.all()
        return points_schema.dump(points)

    def post(self):
        """
        Create a new point from the provided form data and store it
        in the database.

        Returns:
            The newly created point in JSON format if successful.
            An error message and status code if there is an issue
            with the request or database operation.
        """
        app.logger.warning("POST")
        date = request.form.get("date")
        if date:
            try:
                date = datetime.fromisoformat(request.form.get("date"))
            except ValueError as e:
                app.logger.warning(e)
                return f"KeyError! ({e})", 400
        # Serialize new point !
        try:
            new_point = Point(
                element_name=request.form["element_name"],
                latitude=request.form["latitude"],
                longitude=request.form["longitude"],
                comment=request.form.get("comment"),
                date=date,
            )
        except KeyError as e:
            app.logger.warning(e)
            return f"KeyError! ({e})", 400
        except (ValueError, TypeError) as e:
            app.logger.warning(e)
            return f"Bad request ! ({e})", 400
        # Storing date ton
        try:
            db.session.add(new_point)
            db.session.commit()
        except (SQLAlchemyError, IntegrityError) as e:
            app.logger.warning(e)
            return "failed to store in database!", 500
        return point_schema.dump(new_point)


class PointResource(Resource):
    """
    Resource for handling API requests related to a single Point object.

    Methods:
        get: Retrieves a point by its ID and returns it in JSON format.
        delete: Deletes a point by its ID from the database.
    """

    def get(self, point_id):
        """
        Retrieve a point by its ID.

        Args:
            point_id (int): The unique identifier of the point to retrieve.

        Returns:
            The point in JSON format if found.
            A 404 error if the point is not found.
        """
        point = Point.query.get_or_404(point_id)
        return point_schema.dump(point)

    def delete(self, point_id):
        """
        Delete a point by its ID from the database.

        Args:
            point_id (int): The unique identifier of the point to delete.

        Returns:
            An empty string with a 204 status code if the deletion is
            successful.
            A 404 error if the point is not found.
        """
        point = Point.query.get_or_404(point_id)
        db.session.delete(point)
        db.session.commit()
        return "", 204


point_schema = PointSchema()
points_schema = PointSchema(many=True)

api.add_resource(PointListResource, "/api/points/")
api.add_resource(PointResource, "/api/points/<int:point_id>/")


@app.route("/geojson")
def geojson_view():
    """
    View function to generate a GeoJSON representation of all points in the database.

    This function queries all Point objects from the database, organizes them into a catalog
    based on their element_name, and then converts the catalog into a GeoJSON FeatureCollection.
    Points with the same element_name are grouped into LineString geometries, with an additional
    Point geometry for the first position.

    Returns:
        geojson.FeatureCollection: A GeoJSON FeatureCollection representing all points in the database.
    """
    queryset = Point.query.all()
    catalog = {}
    attributes = {}
    for p in queryset:
        url = f"{request.url_root}/api/points/{p.id}/"
        if p.element_name not in catalog.keys():
            catalog[p.element_name] = [(p.longitude, p.latitude)]
            attributes[p.element_name] = {"url": url, "name": p.element_name}
        # Sinon, ajout du point à la clef
        else:
            catalog[p.element_name].append((p.longitude, p.latitude))

    # Transformation du catalog en objet GeoJSON
    elements = []
    for key in catalog.keys():
        e = catalog[key]
        if len(e) > 1:
            # Cas d'un element_name avec plusieurs Point
            # Transformation en LineString
            # + un Point pour le premier élément
            line = geojson.LineString(e)
            point = geojson.Point(e[0])
            # Add trajectory
            feature = geojson.Feature(geometry=line)
            elements.append(feature)
            # Add marker for first position
            feature = geojson.Feature(geometry=point, properties=attributes[key])
            elements.append(feature)
        else:
            # Cas d'un élement
            point = geojson.Point(e[0])
            feature = geojson.Feature(geometry=point, properties=attributes[key])
            elements.append(feature)
    geo_collection = geojson.FeatureCollection(elements)
    return geo_collection


if __name__ == "__main__":
    app.run(host="0.0.0.0")

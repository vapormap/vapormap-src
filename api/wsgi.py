"""
WSGI entry point for the application.

This module contains the WSGI application used by the server
to forward requests to the web application.
"""

from app.app import app

if __name__ == "__main__":
    app.run()

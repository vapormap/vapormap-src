# VaporMap

--- 

## Description

VaporMap est une application web permettant d'effectuer des relevés de points GPS, et de les conserver dans une base de données. 

Ces points sont affichés sur une carte.

> Cette application est une maquette développée rapidement dans un but pédagogique par Tristan Le Toullec.

![vapormap-screenshot](./vapormap-screenshot.png)


---
## Architecture de l'application

L'application "VaporMap" est constituée de 2 parties :

- **L'API** : 
    - Gère les données : création des points de relevé
    - Fourni les informations sur les points au format "geojson" [https://fr.wikipedia.org/wiki/GeoJSON](https://fr.wikipedia.org/wiki/GeoJSON)
    - API au format REST
    - Développée avec le framework Python [Flask](https://flask.palletsprojects.com/), un micro framework open-source de développement web en Python. 

- **Le frontend** : 
    - Interface avec l'utilisateur
    - S'exécute dans le navigateur
    - Composé de fichiers statiques (html, js, css, ...)

L'utilisateur final de l'application interagit avec le frontend.

Le frontend  exécute du code javascript dans le navigateur de l'utilisateur, et effectue des requêtes vers l'API. Il permet à l'utilisateur d'ajouter des points de relevés, et d'afficher les points saisis sur une carte.

L'API est interrogée par le navigateur de l'utilisateur. Les routes définies pour cette API sont :

 - "/geojson" : export de la liste des points au format geojson
 - "/api/points" : api de gestion des points

> *Rem* : l'API peut être utilisée sans le frontend.


---
## Environnement Python

L'utilitaire de gestion de packages python "pip" est utilisé pour gérer les dépendances.

L'utilitaire "venv" est utilisé pour gérer des environnements python dédiés, permettant de lancer l'application dans un environnement indépendant du reste du système.

Pour "publier" le contenu du site web, le module python "http.server" est utilisé.


---
## Principe de fonctionnement du framework Python Flask

Flask permet de déployer les applications dans différents environnements techniques.

Pour Vapormap, les modes "développement" et "production" ont été configurés :

* En "développement", l'application utilise serveur http intégré à Flask, et stocke ses données dans une base sqlite.
* En "production" l'application utilise un serveur web externe (Apache ou Nginx), et stocke ses données dans une base mysql

Quelques commandes utilisées pour la gestion d'une application Flask : 

- `flask db upgrade` : installation ou mise à jour des schémas de base de données (tables, schema)


--- 
## Déploiement

Pour déployer Vapormap en mode développement suivez le guide : [Installation en mode développement](./developpement.md)


---



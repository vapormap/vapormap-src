# Déploiement en mode développement

---
## Description


En mode développement :

- Pour le frontend, le module "http.server" de python est utilisé.
- Pour l'API, le mode "développement" du Framework Flask est utilisé. Dans ce mode Flask intègre directement une base de données et un serveur d'exécution du code python, et de publication web.


La base de données en mode développement est une base de donnée Sqlite3, en mode fichier, stocké en local, et ne nécessitant pas de serveur . Le fichier `db.sqlite3` contenant les données est créé dans le dossier `app/vapormap/datas/`.

Le serveur d'exécution du code python (run) permet de vérifier rapidement le fonctionnement du programme et de développer sans installer un serveur http sur la machine de développement: 

-  Par défaut, le serveur fonctionne sur le port 5000 à l’adresse IP 127.0.0.1.
-  Vous pouvez lui transmettre explicitement une adresse IP et un numéro de port.
-  Le serveur de développement recharge automatiquement le code Python lors de chaque requête si nécessaire. 

**N’UTILISEZ PAS CE SERVEUR DANS UN ENVIRONNEMENT DE PRODUCTION. Il n’a pas fait l’objet d’audits de sécurité ni de tests de performance. 
**

L'installation a été validée dans un environnement Ubuntu 24.04.

---
## Utilisation de localhost, et ouverture de ports

**A la fois le frontend et l'API doivent être accessibles depuis un navigateur. Ce n'est pas le serveur du frontend qui accède à l'API, mais le navigateur.
**

L'API est prévue pour utiliser le port 5000, et le frontend le port 8000. Si ces ports ne sont pas disponibles, vous pouvez en choisir d'autres lors du déploiement.

Si vous installez l'application directement sur votre machine locale, vous n'avez pas de configuration complémentaire à faire. Les 2 serveurs utilisés pour l'API et le frontend utiliseront directement les ports de votre machine, et vous pourrez utiliser le nom de serveur "localhost"

Si vous travaillez avec une machine virtuelle installée sur votre machine locale, vous devez mettre en place des redirection pour ces 2 ports. Vous pouvez utiliser "localhost", qui fait référence à votre machine locale, qui redirige les flux vers votre machine virtuelle. Par exemple avec des "Port Forwarding Rule" si vous utilisez virtualbox.

Si vous travaillez avec une machine distante, vous ne devez pas utiliser "localhost", mais l'adresse IP publique de votre VM, ou de votre instance Cloud. Il peut être nécessaire d'autoriser l'accès distant aux ports 8000 et 5000.

----
## Préparation du système

### Mise à jour du système (optionnel)
``` bash
sudo apt update && sudo apt upgrade -y
# [sudo] password for vapormap: 
# Atteint :1 http://re.archive.ubuntu.com/ubuntu jammy InRelease
# Réception de :2 http://re.archive.ubuntu.com/ubuntu jammy-updates InRelease [119 kB]
# ...
```

### Installation des prérequis (optionnel)
```
sudo apt -y install git sudo vim jq
```

---
## Initialisation de l'application

### Pré-requis Python

Installer  python3, pip3 et venv 
``` bash
sudo apt -y install python3 python3-pip python3-venv
```


Vérifier
``` bash
python3 --version
# Python 3.12.3

pip3 --version
# pip 24.0 from /usr/lib/python3/dist-packages/pip (python 3.12)

python3 -m venv -h
# ...
# Creates virtual Python environments in one or more target directories.
# ...
```


### Récupération du projet
```
cd $HOME
git clone https://gitlab.com/vapormap/vapormap-src.git vapormap-dev
cd vapormap-dev
```

###  Création d'un environnement Python virtuel
``` bash
cd $HOME/vapormap-dev
mkdir venv
python3 -m venv --prompt vapormap-dev ./venv
```

---
## Lancement de l'API

### Initialisation de l'environnement Python
``` bash
cd $HOME/vapormap-dev
source venv/bin/activate
```

### Installation des "requirements" de l'application
```
cd $HOME/vapormap-dev/api
pip install -r requirements/development.txt
```

### Création de la DB sqlite
``` bash
cd $HOME/vapormap-dev/api/app
export SETTINGS_FILE="development"
export FLASK_APP=app
flask db upgrade
# [...] WARNING in app: Overriding base configuration with development.py
# [...] WARNING in app: CORS Allowed domains : *
# INFO  [alembic.runtime.migration] Context impl SQLiteImpl.
# INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
# INFO  [alembic.runtime.migration] Running upgrade  -> dca1b25755c9, empty message
```

### Lancement de l'application
``` bash
export SETTINGS_FILE="development"
cd $HOME/vapormap-dev/api/app
flask run -h 0.0.0.0 -p 5000
# [...] WARNING in app: Overriding base configuration with development.py
# [...] WARNING in app: CORS Allowed domains : *
#  * Serving Flask app 'app'
#  * Debug mode: off
# WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
#  * Running on http://127.0.0.1:5000
#  * Running on http://xxx.xxx.xxx.xxx:5000

# Press CTRL+C to quit

```

### Test

***Accès à l'API en local avec curl***

- Dans une autre fenêtre de terminal
``` bash
curl http://localhost:5000/api/points/?format=json
```
- Réponse
``` json
[]
```

***Accès à l'API en distant, avec un navigateur***

Depuis un navigateur, saisir l'url : [http://localhost:5000/api/points/?format=json](http://localhost:5000/api/points/?format=json)

> Modifiez éventuellement l'url en fonction de votre configuration

La réponse doit être la même qu'avec curl en ligne de commande


---
## Lancement du frontend 

### Ouvrir un autre terminal, et initialiser l'environnement python
``` bash
cd $HOME/vapormap-dev
source venv/bin/activate
```

### Configurer l'accès à l'API
``` bash
cd $HOME/vapormap-dev/frontend/
export VAPORMAP_BACKEND_HOST=<PUBLIC_API_ENDPOINT_HOST>
export VAPORMAP_BACKEND_PORT=5000
envsubst '${VAPORMAP_BACKEND_HOST},${VAPORMAP_BACKEND_PORT}' < config.json.template > html/config.json
```
Attention, pour `VAPORMAP_BACKEND_HOST` vous devez indiquer une adresse accessible depuis le navigateur. `localhost` ne fonctionne que si vous travaillez sur votre machine locale, sinon il faut indiquer l'adresse IP publique de votre VM, ou de votre instance Cloud.


Vérifier le contenu du fichier 
``` bash
cat html/config.json
```


### Lancer le frontend
``` bash
cd $HOME/vapormap-dev/frontend/
cd html
python -m http.server
```

### Test de l'accès à l'application

***Accès à l'API en local avec curl***

``` bash
curl http://localhost:8000
```

Le code d'une page html contenant les scripts de l'application s'affiche. Il est nécessaire d'utiliser un navigateur pouvant interpréter ce code pour pouvoir valider le bon fonctionnement de l'application : 

***Accès à l'API en distant, avec un navigateur***

Depuis un navigateur, saisir l'url : [http://localhost:8000](http://localhost:8000)

> Modifiez éventuellement l'url en fonction de votre configuration


L'application doit afficher une carte du monde, et permettre la saisie de points de relevé :

- Pour saisir un point de relevé :
    - Cliquer sur un point dans la carte, ce qui va renseigner les champs **Lat**itude et **Long**itude
    - Saisir les données complémentaires de description (**What ?**), et de **Date**
    - Cliquer sur **Add**.
    - Un marqueur de relevé ![](./frontend/static/assets/images/marker-icon.png) doit apparaître sur la carte.
- Si 2 points de relevé sont saisis avec le même champ de description, un trajet est créé. 
- Si aucun marqueur n’apparaît, le lien entre le frontend et l'API ne fonctionne pas:
    - Vérifier que l'API est bien fonctionnelle, depuis le serveur d'API
    - Vérifier que l'url d'accès de l'API est correcte dans le fichier `frontend/config.json`
    - Vérifier que l'API est joignable depuis le poste client



Les requêtes vers les serveurs apparaissent dans les fenêtres de terminal
- Pour le frontend
``` bash
# 10.0.2.2 - - [date] "GET / HTTP/1.1" 200 -
# 10.0.2.2 - - [date] "GET /static/assets/leaflet.css HTTP/1.1" 200 -
# 10.0.2.2 - - [date] "GET /static/assets/bootstrap.css HTTP/1.1" 200 -
# 10.0.2.2 - - [date] "GET /static/assets/jquery.min.js HTTP/1.1" 200 -
#  ...
# 10.0.2.2 - - [date] "GET /config.json HTTP/1.1" 200 -
#  ...
# 10.0.2.2 - - [date] "GET /favicon.ico HTTP/1.1" 200 -
#  ...
```
- Pour l'API
``` bash
# 10.0.2.2 - - [date] "GET /favicon.ico HTTP/1.1" 404 -
# 10.0.2.2 - - [date] "GET /api/points/ HTTP/1.1" 200 -
# 10.0.2.2 - - [date] "GET /geojson HTTP/1.1" 200 -
#  ...
```


---
## Arrêt de l'application

***Arrêt du Frontend***

Pour arrêter le serveur frontend : dans le terminal utilisé pour lancer le frontend, saisir ++control+c++

Puis sortir de l'environnement virtuel 
``` bash
deactivate
```


***Arrêt de l'API***

Pour arrêter le serveur frontend : dans le terminal utilisé pour lancer l'API, saisir ++control+c++

Puis sortir de l'environnement virtuel 
``` bash
deactivate
```



